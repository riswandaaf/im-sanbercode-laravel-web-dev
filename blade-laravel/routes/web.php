<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/',[HomeController::class, 'home']);

Route::get('/register',[AuthController::class, 'register']);

Route::post('/home',[AuthController::class, 'welcome']);

Route::get('/master',function(){
    return view('layouts.master');
});

Route::get('/dataTable',function(){
    return view('page.dataTable');
});

Route::get('/table',function(){
    return view('page.table');
});

//CRUD CastController
//Create
//Form tambah data
Route::get('/cast/create',[CastController::class,'create']);
//Kirim data ke database atau tambah data ke database
Route::post('/cast',[CastController::class,'store']);
