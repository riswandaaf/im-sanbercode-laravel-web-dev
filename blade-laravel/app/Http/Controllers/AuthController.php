<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page.register');
    }

    public function welcome(Request $request){
        // dd($request);
        $firstName = $request->input('FirstName');
        $lastName = $request->input('LastName');
        $bio = $request->input('Bio');

        return view('page.welcome',[
            'FirstName'=>$firstName,
            'LastName' => $lastName,
            'Bio' => $bio
        ]
    );
    }
}
