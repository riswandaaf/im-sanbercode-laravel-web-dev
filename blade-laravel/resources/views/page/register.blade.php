<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <!-- Name  -->
        <label for="FirstName">First Name:</label><br>
        <input type="text" id="FirstName" name="FirstName"><br><br>

        <label for="LastName">Last Name:</label><br>
        <input type="text" id="LastName" name="LastName"><br><br>

        <!-- Gender  -->
        <label for="Gender">Gender:</label><br>
            <input type="radio" id="male" name="gender" value="male">
                <label for="male">Male</label><br>
            <input type="radio" id="Female" name="gender" value="Female">
                <label for="Female">Female</label><br>
            <input type="radio" id="Other" name="gender" value="Other">
                <label for="Other">Other</label><br><br>
        
        <!-- Dropdown  -->
        <label for="Nationality">Nationality</label>
        <select id="Nationality" name="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Australia">Australia</option>
        </select><br><br>

        <!-- Language Spoken  -->
        <label for="Language">Language Spoken:</label><br>
        <input type="checkbox" id="BahasaIndonesia" name="language">
        <label for="language">Bahasa Indonesia</label><br>
        <input type="checkbox" id="English" name="language">
        <label for="language">English</label><br>
        <input type="checkbox" id="Other" name="language">
        <label for="language">Other</label><br><br>

        <label for="bio">Bio:</label><br>
        <textarea  rows="10" cols="30" id="bio" name="Bio"></textarea><br>

        <!-- button submit a href  -->
        <a href="/welcome">
            <button type="submit">Sign Up</button>
        </a>
        
    </form>

</body>
</html>