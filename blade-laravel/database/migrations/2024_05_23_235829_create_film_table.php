<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('film', function (Blueprint $table) {
            $table->bigIncrements('id'); // id bigIncrement
            $table->string('judul', 45); // judul VARCHAR(45)
            $table->text('ringkasan'); // ringkasan text
            $table->integer('tahun'); // tahun INT
            $table->string('poster', 45)->nullable(); // poster VARCHAR(45)
            $table->unsignedBigInteger('genre_id'); // genre_id INT foreign key

            // Define foreign key constraint
            $table->foreign('genre_id')->references('id')->on('genre')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('film');
    }
};
