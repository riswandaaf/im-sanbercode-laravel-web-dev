<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kritik', function (Blueprint $table) {
            $table->bigIncrements('id'); // id BigIncrement
            $table->unsignedBigInteger('user_id'); // user_id INT pivot dari tabel user
            $table->unsignedBigInteger('film_id'); // film_id INT pivot dari tabel film
            $table->text('content'); // content TEXT
            $table->integer('point'); // point INT

            // Define foreign key constraints
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
            $table->foreign('film_id')->references('id')->on('film')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kritik');
    }
};
