<?php
// Ape.php

require_once 'Animal.php';

class Ape extends Animal {
    // Constructor
    public function __construct($name) {
        parent::__construct($name); // Memanggil constructor dari parent class
        $this->legs = 2; // Override jumlah kaki
    }

    // Metode untuk menjalankan perilaku khusus kera
    public function yell() {
        return "Auooo";
    }
}
?>
