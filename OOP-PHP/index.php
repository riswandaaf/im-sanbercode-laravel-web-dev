<?php

require 'ape.php';
require 'frog.php';

// use Animals\frog;
// use Animals\ape;

$sheep = new Animal("shaun");

echo "name : $sheep->name<br>";
echo "legs : $sheep->legs <br>";
echo "blooded : $sheep->cold_blooded <br>"; // "no"
echo "<br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


$sungokong = new Ape("kera sakti");
echo "name : $sungokong->name<br>";
echo "legs : $sungokong->legs <br>";
echo "yell : ".$sungokong->yell()."<br>"; 
echo "<br>";// "Auooo"

$kodok = new Frog("buduk");
echo "name : $kodok->name<br>";
echo "legs : $kodok->legs <br>";
echo "yell : ".$kodok->jump()."<br>"; 
echo "<br>";// "Auooo"

?>