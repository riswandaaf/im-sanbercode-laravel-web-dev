<?php
// namespace Animals;
class Animal {
    // Deklarasi properti
    public $name;
    public $legs;
    public $cold_blooded;

    // Constructor
    public function __construct($name) {
        $this->name = $name;
        $this->legs = 4; // Default value
        $this->cold_blooded = 'no'; // Default value
    }

    // function untuk mendapatkan nama hewan
    public function getName() {
        return $this->name;
    }

    // function untuk mendapatkan informasi hewan
    public function getInfo() {
        return "Animal: $this->name, Legs: $this->legs, Cold-blooded: $this->cold_blooded";
    }
}
?>
